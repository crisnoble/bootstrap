var AlertModel = Backbone.Model.extend({});

var alertModel = new AlertModel(
	{text: 'Some text', status: 'alert-error'}
);

var AlertView = Backbone.View.extend({

	events: {
		"click a": "remove"
	},
	initialize: function(){
		this.model.on('change', this.render, this);
		this.model.on('destroy', this.remove, this);
	},
	className: 'alert',
	template: _.template(
		'<a class="close" href="#">&times;</a>' + 
		'<%= text %>'
		),
	render: function(){
		$('.alert').remove();
		var attributes = this.model.toJSON();
		this.$el.html(this.template(attributes));
		this.$el.addClass(this.model.get('status'));
		$('#content').prepend(this.$el);
	},
	remove: function(){
		this.$el.remove();
	}

});

var DropDownView = Backbone.View.extend({

	tagName: "li",
	template: _.template(
		'<a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>' +
        '<ul class="dropdown-menu">' +
            '<li><a href="#">Action</a></li>' +
            '<li><a href="#">Another action</a></li>' +
            '<li><a href="#">Something else here</a></li>' +
            '<li class="divider"></li>' +
            '<li class="nav-header">Nav header</li>' +
            '<li><a href="#">Separated link</a></li>' +
            '<li><a href="#">One more separated link</a></li>' +
        '</ul>'
	),
	className: 'dropdown',
	render: function(){
		this.$el.show();
		this.$el.html(this.template());
		$('#navigation').append(this.$el);
	},
	remove: function(){
		this.$el.hide();
	}

});

var SignOutLinkView = Backbone.View.extend({

	events: {
		"click": "signOut"
	}, 
	tagName: "a",
	id: "signOutLink",
	template: _.template(
		'Signout'
	),
	render: function(){
		this.$el.attr('style','display:inline-block');
		this.$el.html(this.template());
		$('#rightNav').append(this.$el);
	},
	remove: function(){
		this.$el.attr('style','display:none');
	},

	signOut: function(e){
		app.signOut(newAlert);
		userUICheck();
	}

});

var LogInFormView = Backbone.View.extend({
	events: {
		"click button":"logIn"
	},
	template: _.template(
		'<input class="span2 logInForm" type="text" placeholder="Email" id="email">' +
		'<input class="span2 logInForm" type="password" placeholder="Password" id="password">' +
		'<button type="submit" class="btn logInForm" id="submit">Sign in</button>'
	),
	render: function() {
		this.$el.show();
		this.$el.html(this.template());
		$('#rightNav').append(this.$el);
	},
	remove: function() {
		this.$el.hide();
	},
	logIn: function(e) {
		e.preventDefault();
		var credentials = {};
		credentials.username = this.$el.find('#email').val();
		credentials.password = this.$el.find('#password').val();
		if (credentials.username !== '' && credentials.password !== ''){
			app.signIn(credentials, logInSuccess, logInFail);
		} else {
			newAlert('Looks like you forgot your credentials.', 'alert-error');
		}
	}

});

dropDownView = new DropDownView();
signOutLinkView = new SignOutLinkView();
logInFormView = new LogInFormView();
