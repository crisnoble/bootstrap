It was fairly trivial to implement the functionality using jQuery.

However, this seemed too easy. It seemed from the phone interview with Justin that this would  be a backbone role. I am much more well versed with html and css so I had a choice to make: try to show off my css and make this bootstrap template look nice or try to re-factor the code using backbone.js, it might not be pretty but I learned a lot along the way.

The first thing I did was create a model and view for the alert. I think I came up with a decent and flexible solution. Instead of creating a jQuery function that renders and appends html, I used the underscore template library to create the view. The view will update if a new alert is pushed to the model the alert will show up in the proper color.

I then created three views, one for the 'Sign Out Link', one for the 'Login Form' and one for the 'Drop Down'. The userUICheck() essentially toggles these views based on whether or not the user is actually signed in. Unfortunately I didn't feel like I was improving the code vs. the jQuery very much with these but it was still a fun challenge. 

I also added a small (almost useless) validation function into the LogInFormView which ensures that the username and password fields are at least not empty before attempting to sign in.

https://bitbucket.org/crisnoble/bootstrap/src

Thank you so much for the challenge, I know that I learned a lot. Hopefully, you will see in my code a front-end guy who wants to learn more and more about backbone.js.